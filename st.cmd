#
# Module: essioc
#
require essioc

#
# Module: cabtr
#
require cabtr


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${cabtr_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-TC-003
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_monitor.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TC-003, IPADDR = ts2-cabtr-03.tn.esss.lu.se, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-033
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-033, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 1, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-034
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-034, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 2, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-043
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-043, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 3, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-044
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-044, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 4, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-042
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-042, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 5, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-048
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-048, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 6, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-021
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-021, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 7, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-057
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-057, CONTROLLER = TS2-010CRM:Cryo-TC-003, CHANNEL = 8, POLL = 1000")
